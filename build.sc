import $file.webpack
import $file.`laminar-web-components`.generator.generator
import mill.define.Target
import mill._, scalalib._, modules._
import coursier.maven.MavenRepository
import coursier.core.Authentication
import $ivy.`com.lihaoyi::mill-contrib-bloop:$MILL_VERSION`
import scala.util.chaining._
import scalajslib.ScalaJSModule
object V {
  val scala = "3.0.2"
  val scalaJS = "1.7.0"
  val zio = "1.0.11"
  val laminar = "0.13.0-M1"
  val http4s = "0.23.3"
  val sttp = "3.3.15"
  val laminarWCM = "0.11.0"
  val doobie = "1.0.0-RC1"
  val catsEffect = "3.2.9"
  val tapir = "0.19.0-M12"
  val circe = "0.15.0-M1"
}

object backend extends ScalaModule {
  def scalaVersion = V.scala

  def copyJS = T(
    resources().head.path
      .tap(jsPath =>
        os.list(frontend.devWebpack().head.path / os.up)
          .filter(_.last.startsWith("out"))
          .foreach(f =>
            (jsPath / f.last)
              .tap(oF => if (os.exists(oF)) os.remove(oF))
              .pipe(os.copy(f, _))
          )
      )
      .pipe(PathRef(_))
  )
  override def compile = T {
    copyJS()
    super.compile()
  }

  def ivyDeps = Agg(
    ivy"org.http4s::http4s-blaze-server:${V.http4s}",
    ivy"org.http4s::http4s-dsl:${V.http4s}",
    ivy"org.http4s::http4s-circe:${V.http4s}",
    ivy"dev.zio::zio:${V.zio}",
    ivy"dev.zio::zio-interop-cats:3.1.1.0",
    ivy"org.tpolecat::doobie-core:${V.doobie}",
    ivy"org.tpolecat::doobie-h2:${V.doobie}",
    ivy"org.typelevel::cats-effect:${V.catsEffect}"
  )

  def moduleDeps = Seq(sharedJVM)

}

trait SharedBase extends ScalaModule {
  override def millSourcePath = os.pwd / "shared"
  def scalaVersion = V.scala
}

object sharedJS extends ScalaJSModule with SharedBase {
  override def millSourcePath = os.pwd / "shared"
  def scalaVersion = V.scala
  def scalaJSVersion = V.scalaJS

  def ivyDeps = Agg(
    ivy"io.circe::circe-core::${V.circe}"
    // ivy"com.softwaremill.sttp.tapir::tapir-core::${V.tapir}",
    // ivy"com.softwaremill.sttp.tapir::tapir-json-circe::${V.tapir}"
  )
}

object sharedJVM extends SharedBase {
  def ivyDeps = Agg(
    ivy"io.circe::circe-core:${V.circe}"
  )
}

object frontend extends webpack.WebpackLib.ScalaJSWebpackApplicationModule {
  import scalajslib._
  import scalajslib.api._

  def scalaVersion = V.scala
  def scalaJSVersion = V.scalaJS
  def ivyDeps = T {
    buildMaterialBindings()
    Agg(
      ivy"com.raquo::laminar::0.13.1",
      ivy"com.github.uosis::laminar-web-components-material::${V.laminarWCM}",
      ivy"com.softwaremill.sttp.client3::core::${V.sttp}",
      ivy"com.softwaremill.sttp.client3::circe::${V.sttp}"
    )
  }

  def moduleDeps = Seq(sharedJS)

  def buildMaterialBindings = T {
    val x = generator.generateMaterial(T.dest)
    val call = os.proc("sbt", "+publishLocal").call(cwd = T.dest)
    if (call.exitCode != 0) throw new Exception(call.out.string)
    x
  }

  override def moduleKind = ModuleKind.CommonJSModule
}
