package io.gitlab.mhammons

import io.circe.*

type PomodoroLevels = 1 | 2 | 3 | 4

extension (pl: PomodoroLevels)
  def increment: PomodoroLevels = pl match
    case 1 => 2
    case 2 => 3
    case 3 => 4
    case 4 => 1

given Encoder[PomodoroLevels] =
  Encoder.encodeInt.contramap[PomodoroLevels](identity)
given Decoder[PomodoroLevels] = Decoder.decodeInt.emap {
  case l @ (1 | 2 | 3 | 4) => Right(l)
  case l                   => Left("level was not 1-4")
}

// given Schema[PomodoroLevels] = Schema.schemaForInt.map[PomodoroLevels] {
//   case l @ (1 | 2 | 3 | 4) => Some(l)
//   case l                   => None
// }(identity)
