package io.gitlab.mhammons

import scala.concurrent.duration.FiniteDuration
import io.circe.Codec
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit.MILLIS
import scala.concurrent.duration.*
import io.circe.Encoder
import io.circe.Decoder
import scala.util.chaining.*

case class PomodoroState(
    phase: PomodoroPhase,
    remainingMs: Long,
    paused: Boolean
) derives Codec.AsObject:
  val remaining: FiniteDuration = FiniteDuration(remainingMs, "millis")
  def remainingPct: Double = 1 - (remaining / phase.duration)

object PomodoroState:
  def apply(phase: PomodoroPhase, paused: Boolean = true): PomodoroState =
    PomodoroState(phase, phase.duration.toMillis, paused)
