package io.gitlab.mhammons

import io.circe.*
import scala.concurrent.duration.*

enum PomodoroPhase(level: PomodoroLevels) derives Codec.AsObject:
  private final val shortRest = 5.minutes
  private final val longRest = 20.minutes
  private final val work = 25.minutes

  def duration = this match
    case Rest(1 | 2 | 3) => shortRest
    case Rest(4)         => longRest
    case Work(_)         => work

  def nextPhase = this match
    case Rest(n) => Work(n.increment)
    case Work(n) => Rest(n)
  case Rest(level: PomodoroLevels) extends PomodoroPhase(level)
  case Work(level: PomodoroLevels) extends PomodoroPhase(level)
