import cats.effect.*
import cats.syntax.all.*
import org.http4s.*
import org.http4s.dsl.io.*
import org.http4s.implicits.*
import io.gitlab.mhammons.PomodoroState
import org.http4s.circe.*
import scala.util.chaining.*
import io.circe.syntax.*
import org.http4s.circe.CirceEntityCodec.*

def getPomodoroState(ref: Ref[IO, PomodoroRecord]) =
  ref.get.map(_.toPomodoroState).flatMap(Ok(_))

def setPomodoroState(ref: Ref[IO, PomodoroRecord], ps: PomodoroState) =
  ps.pipe(PomodoroRecord.fromPomodoroState).pipe(ref.set).flatMap(_ => Ok(""))

def service(ref: Ref[IO, PomodoroRecord]) = HttpRoutes.of[IO] {
  case GET -> Root / "pomodoro" => getPomodoroState(ref)
  case req @ PUT -> Root / "pomodoro" =>
    for
      state <- req.as[PomodoroState]
      resp <- setPomodoroState(ref, state)
    yield resp

  case request @ GET -> Root / f =>
    StaticFile
      .fromResource(f, Some(request))
      .getOrElseF(NotFound())

  case request @ GET -> Root =>
    StaticFile.fromResource("index.html", Some(request)).getOrElseF(NotFound())
}
