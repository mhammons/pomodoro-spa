import org.http4s.server.Server
import org.http4s.blaze.server.BlazeServerBuilder
import zio.interop.catz.*
import org.http4s.server.staticcontent.*
import cats.effect.{IOApp, IO, ExitCode}
import scala.concurrent.ExecutionContext
import cats.effect.kernel.Resource
import org.http4s.server.{Router, Server => BlazeServer}
import cats.effect.kernel.Ref
import io.gitlab.mhammons.PomodoroPhase
import cats.implicits.*
import scala.util.chaining.*

object HelloWorld extends IOApp:

  def run(args: List[String]): IO[ExitCode] = for
    ref <- Ref[IO].of(
      PomodoroPhase
        .Work(1)
        .pipe(phase => PomodoroRecord.Waiting(phase, phase.duration))
    )
    server <- BlazeServerBuilder[IO](runtime.compute)
      .bindHttp(8080)
      .withHttpApp(service(ref).orNotFound)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  yield server
