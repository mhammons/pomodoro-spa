import io.gitlab.mhammons.PomodoroPhase
import scala.concurrent.duration.FiniteDuration
import java.time.LocalDateTime
import io.gitlab.mhammons.PomodoroState
import scala.concurrent.duration.*
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit.MILLIS
import scala.util.chaining.*

enum PomodoroRecord:
  def toPomodoroState: PomodoroState = this match
    case Waiting(phase, remaining) =>
      PomodoroState(phase, remaining.toMillis, true)
    case Endable(phase, endtime) if LocalDateTime.now.isBefore(endtime) =>
      PomodoroState(
        phase,
        LocalDateTime.now.until(endtime, MILLIS),
        false
      )
    case Endable(phase, _) =>
      phase.nextPhase.pipe(nPhase =>
        PomodoroState(nPhase, nPhase.duration.toMillis, true)
      )

  case Waiting(phase: PomodoroPhase, remaining: FiniteDuration)

  case Endable(phase: PomodoroPhase, endTime: LocalDateTime)

object PomodoroRecord:
  def fromPomodoroState(pomodoroState: PomodoroState): PomodoroRecord =
    if pomodoroState.paused then
      Waiting(pomodoroState.phase, pomodoroState.remainingMs.millis)
    else
      Endable(
        pomodoroState.phase,
        LocalDateTime.now.plus(pomodoroState.remainingMs, MILLIS)
      )
