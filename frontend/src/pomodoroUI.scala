import org.scalajs.dom
import com.raquo.laminar.api.L.*
import com.github.uosis.laminar.webcomponents.material.Button
import scala.concurrent.duration.{span => dspan, *}
import scala.util.chaining.*
import com.raquo.airstream.core.Observable
import com.github.uosis.laminar.webcomponents.material.CircularProgress
import com.github.uosis.laminar.webcomponents.material.LinearProgress
import com.github.uosis.laminar.webcomponents.material.styles
import org.scalajs.dom.raw.UIEvent

import io.gitlab.mhammons.PomodoroState
import io.gitlab.mhammons.PomodoroPhase
import sttp.client3.*
import sttp.client3.circe.*
import scala.concurrent.ExecutionContext.Implicits.global
import io.circe.syntax.*
import org.scalajs.dom.window

val backend = FetchBackend()

val host = window.location.host

val getPomState = basicRequest
  .get(uri"http://$host/pomodoro")
  .response(asJson[PomodoroState])

def setPomState(state: PomodoroState) =
  basicRequest.put(uri"http://$host/pomodoro").body(state.asJson)

val pomodoroState: Var[PomodoroState] = PomodoroPhase
  .Work(1)
  .pipe(phase => PomodoroState(phase, phase.duration.toMillis, true))
  .pipe(Var(_))

val pomodoroUpdater = EventStream
  .periodic(500)
  .flatMap(_ =>
    EventStream.fromFuture(getPomState.send(backend).map(_.body.getOrElse(???)))
  )

val rootElement = div(
  div(
    pomodoroUpdater --> pomodoroState.writer,
    h2(
      child.text <-- pomodoroState.signal.map(_.phase.toString),
      textAlign := "center",
      fontFamily := "'Ubuntu Condensed', sans-serif"
    ),
    h1(
      child.text <-- pomodoroState.signal.map(_.remaining.toMinutes.toString),
      "m ",
      child.text <-- pomodoroState.signal
        .map(_.remaining.toSeconds % 60)
        .map(_.formatted("%02d")),
      "s",
      textAlign := "center",
      fontFamily := "'Ubuntu Condensed', sans-serif"
    ),
    LinearProgress(
      _.progress <-- pomodoroState.signal.map(_.remainingPct),
      _ => styles.themePrimary := "blue"
    ),
    br(),
    div(
      Button(
        _.label <-- pomodoroState.signal.map(t =>
          if t.paused then "unpause" else "pause"
        ),
        _.raised <-- pomodoroState.signal.map(_.paused),
        _.styles.themePrimary := "green",
        _ =>
          onClick --> (_ =>
            pomodoroState
              .now()
              .pipe(ps => ps.copy(paused = !ps.paused))
              .pipe(setPomState)
              .send(backend)
          )
      ),
      Button(
        _.label := "skip",
        _ => styles.themePrimary := "blue",
        _ =>
          onClick --> (_ =>
            pomodoroState
              .now()
              .pipe(ps =>
                ps.copy(
                  phase = ps.phase.nextPhase,
                  remainingMs = ps.phase.nextPhase.duration.toMillis
                )
              )
              .pipe(setPomState)
              .send(backend)
          )
      ),
      display := "flex",
      justifyContent := "space-around"
    ),
    display := "flex",
    flexDirection := "column",
    justifyContent := "center",
    minWidth := "30em",
    width := "30%"
  )
)

@main def load = documentEvents.onDomContentLoaded.foreach(_ =>
  render(dom.document.querySelector("#appContainer"), rootElement)
)(using unsafeWindowOwner)
