# Pomodoro Single Page Application

This is an example [Pomodoro Timer](https://todoist.com/productivity-methods/pomodoro-technique). It uses Scala 3, ScalaJS, HTTP4s, Cats-effect, STTP, Laminar, and Mill to construct the frontend javascript application and the backend server.

This repository is meant to be used in VSCode. The dockerfile within can be used to construct a development environment to work in. Look at VSCode's Remote Development extensions for further details.

The Mill project definition is split into four modules:
* backend
* frontend
* sharedJS
* sharedJVM

The sharedJS and sharedJVM modules both point to the shared folder and its sources, and the respective modules are used as dependencies for the frontend and backend respectively. The frontend module has a custom task tied to its compilation that runs sbt publishLocal on a forked version of the laminar-web-components project the first time you compile it. The backend module depends on the output javascript of the frontend module, copying it into the backend resources for the HTTP4s server to serve alongside the base `index.html` page.

To launch the server in a development mode, use `mill -w backend.runBackground`. This will make the server recompile and redeploy upon source changes. Once the server is running, navigate to `http://localhost:8080` use it. The pomodoro timer and skipping/pausing/unpausing is kept relatively in sync between multiple instances of the page.

This project is a learning project for a home website which my wife can view and then see if I'm currently in the middle of a Pomodoro session or if I'm taking a break.